#! /usr/bin/env python

def humanize(word):
    lower_case_word = word.lower()
    new_word = "%c%s" % (lower_case_word[0].upper(), lower_case_word[1:])
    return new_word

words_file = open("copied_words.txt", "r")
words_js = open("js/words.js", "w")

word_file_string = words_file.read()
words_file.close()
word_file_lines = word_file_string.split("\n")
words = []
sentences = []
dictionary = {}

for word_line in word_file_lines:
    if len(word_line) > 0:
        word_line_array = word_line.split(".mp4")
        hash_word_list = word_line_array[0].split(" ")
        if len(hash_word_list) > 1:
            # print hash_word_list
            is_sentence = True
        else:
            is_sentence = False
        hash_word = "_".join(hash_word_list)
        obj = {}
        obj['key'] = hash_word
        obj['video'] = "%s.mp4" % (word_line_array[0])
        # TODO: Extract the thumbnails from the images uploaded by the user
        obj['thumbnail'] = "%s.jpg" % (word_line_array[0])
        obj['humanized'] = humanize(word_line_array[0])
        # print obj['video']

        if is_sentence:
            sentences.append(obj)
        else:
            words.append(obj)

    dictionary['words'] = words
    dictionary['sentences'] = sentences

# print dictionary
# Close the words_js
string = "window.DICTIONARY = %s;" % (dictionary)
words_js.write(string)
words_js.close()
