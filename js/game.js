
/**
 *  The constants used to define the type of game
 * 
 * @type Number
 */
/*window.GAME.WORD = 0;
window.GAME.SENTENCE = 1;
window.GAME.POSSIBLE_ANSWERS = 3;*/
var WORD = 0;
var SENTENCE = 1;
var POSSIBLE_ANSWERS = 3;


/**
 * A question object that can be used to verify the user's answer
 * based on the type of game that the user is playing.
 * 
 * @param {Object} dictionaryItem The word/sentence that would be retrieved from the dictionary
 * @params {Array} possibleAnswers The list of other possible answers(integers) including the right answer
 * @params {int} correctAnswer The index of this question which should be seen as the correct answer
 * @returns {Question}
 */
function Question(dictionaryItem, possibleAnswers, correctAnswer) {
    
        var dictionaryObject = dictionaryItem;
        var answers = possibleAnswers;
        var rightAnswer = correctAnswer;
        var answeredThis = false;
    
        /**
         * Returns the word/sentence that was chosen from the 
         * dictionary. Use this for text based question
         * 
         * @returns {string}
         */
        this.textBased = function () {
            return dictionaryObject.humanized;
        };
        
        /**
         * Returns the relative url of the video.
         * 
         * @returns {string} The link to the video
         */
        this.videoBased = function() {
            return ['videos', dictionaryObject.video].join("/");
        };
        
        /**
         * Returns the right answer to this question
         * 
         * @returns {int}
         */
        this.answer = function() {
            return rightAnswer;
        };
        
        /**
         * Use this method to test the answer chosen by a user for correctness
         * 
         * @param {int} answer
         * @returns {boolean}
         */
        this.isValidAnswer = function (answer) {
            return answer === rightAnswer;
        };
        
        this.markAsAnswered = function () {
            
        };
        
        this.answered = function () {
            return answeredThis;
        };
        
}

/**
 * 
 * @param {int} gameType The type of game, word or sentence
 * @returns {Game}
 */
function Game(gameType) {
    
      /**
       * Takes an array and shuffles it and returns the shuffled version.
       * 
       * @param {Array} array
       * @returns {Array}
       */
      function shuffle(array) {
          
          var arrayLength = array.length;
          var shuffledArray = array.slice();
          var first = 0, second = 0;
          
          for (var i = 0; i < arrayLength; i++) {
              for (var j = 0; j < arrayLength; j++) {
                  
                  //there is an infinite loop here.
                  //i hava to change this
                  do {
                    //first = parseInt(Math.random() * arrayLength) % arrayLength;
                    //second = parseInt(Math.random() * arrayLength) % arrayLength;
                    //alert("f " + first + " " + "second " + second);
                    first = (Math.random() * arrayLength) % arrayLength;
                    second = (Math.random() * arrayLength) % arrayLength;
                    
                  } while (first === second);
                  
                    first = Math.floor(first);
                    second = Math.floor(second);
                  //alert("f " + Math.floor(first) + " " + "s " + Math.floor(second));
                  
                  //I should have two random numbers, swap the values at those index
                  var temp = shuffledArray[first];
                  shuffledArray[first] = shuffledArray[second];
                  shuffledArray[second] = temp;
              }
          }
          
          return shuffledArray;
          
      }
      
      /**
       * Takes the list with the index of the asked element, then selects
       * other indexes which can be used as possible answers to the given
       * question. The index of the correct answers is also included in
       * this list of possible answers to return but only one will be correct.
       * 
       * @param {Array} array
       * @param {int} index
       * @returns {Array}
       */
      function possibleFalseAnswers(array, index) {
          
          var possibleAnswers = [index];
          var answer;
          var arrayLength = array.length;
          for (var i = 0; i < window.GAME.POSSIBLE_ANSWERS - 1; i++) {
              do {
                  answer = parseInt(Math.random() * arrayLength) % arrayLength;
              } while (possibleAnswers.indexOf(answer) !== -1);
              
              possibleAnswers.push(answer);
          }
          
          var shuffledPossibleAnswers = shuffle(possibleAnswers);
          
          return shuffledPossibleAnswers;
          
      };
    
      // I slice the DICTIONARY array which in this case returns a copy so I don't 
      // mistakenly change my DICTIONARY.
      var list;
      if (gameType === window.GAME.WORD) {
          list = window.DICTIONARY.words.slice();
      } else {
          list = window.DICTIONARY.sentences.slice();
      }

      list = shuffle(list);
      var questions = [];
      
      //Create question objects
      var question, possibleAnswers;
      for (var i = 0; i < list.length; i++) {
          possibleAnswers = possibleFalseAnswers(list, i);
          question = new Question(list[i], i);
          questions.push(question);
      }
    
      /**
       * This is the number of possible questions. It is based on the number
       * of words that are in the dictionary.
       * 
       * @returns int
       */
      this.numberOfQuestions = function () {
          return list.length;
      };
      
      this.getQuestion = function(i) {
          var index = Math.abs(i) % numberOfQuestions();
          return list[index];
      };
      
      this.getQuestions = function () {
        return questions;  
      };
         
}

$(document).ready(function() {
    
    window.GAME = "WORD";
    Game(window.GAME);
    var questions = getQuestions();
     alert( window.DICTIONARY['sentences'].length);
     alert( window.DICTIONARY['words'].length);
    //alert( window.DICTIONARY['sentences'][0]['humanized']);
    //alert( window.DICTIONARY['words'][0]['humanized']);
     //alert( window.DICTIONARY['words']);
     //alert("getQuestions");

});
