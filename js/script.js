var questions = new Array();
var questions_SRC = new Array();
var generated_Numbers = new Array();
var generated_Numbers_count = 0;
var number_generated = false;
var QUESTIONCOUNT = 21;
var current_question = 0;
var current_solution = 0;

$(document).ready(function() {
    
    selectQuestions();
    displayVideo();
    displayQuestions(current_question);
   
    //check if this option was clicked
    //if the option is correct, load the correct symbol
    //if the option is not correct load the incorrect symbol
    $("#optionA").click(function () {
       optionAValue = 0 + current_question;
     
        // alert(optionAValue + " " +current_solution);
      
        if(current_solution === optionAValue)
        {
            loadCorrectSymbol("optionA");
            //disableOptions("optionA","optionC");
        }
        else
        {
           loadInCorrectSymbol("optionA");
        }
    });

    $("#optionB").click(function () {
      optionBValue = 1 + current_question;
       
        //alert(optionBValue +" " +current_solution);
        
        if(current_solution === optionBValue)
        {
            loadCorrectSymbol("optionB");
            //disableOptions("optionA","optionC");
         }
        else
        {
           loadInCorrectSymbol("optionB");
        }
    });

    $("#optionC").click(function () {
        optionCValue = 2 + current_question;
        
        //alert(optionCValue +" " +current_solution);
        
        if(current_solution === optionCValue)
        {
            loadCorrectSymbol("optionC");
            //disableOptions("optionA","optionB");
        }
        else
        {
           loadInCorrectSymbol("optionC");
        }
    });
    
    $("#nextButton").click(function () {
        current_question = current_question +3;
        removeVideo();
        loadNextSetQuestions();
    });

});

function loadNextSetQuestions()
{
    if( current_question < QUESTIONCOUNT  )
    {
        displayVideo();
        changeOptions(current_question);
    }
    else
        alert("all questions done");
}

function selectQuestions()
{
    // there are 64 words but we can only use 63(63/3 = 21)
    for(var i = 0; i < QUESTIONCOUNT;i++)
    {
        do{
            number_generated = false;
            random_number = (Math.random() * QUESTIONCOUNT) + 0 ;
            random_number = Math.floor(random_number);
            
            for(var x = 0; x < generated_Numbers_count;x++)
            {
                if(random_number ===  generated_Numbers[x])
                    number_generated = true;
            }     
        }while(number_generated === true);
        generated_Numbers[generated_Numbers_count] = random_number;
        generated_Numbers_count++;
        questions[i] = window.DICTIONARY['words'][random_number]['humanized'];
        questions_SRC[i] = window.DICTIONARY['words'][random_number]['video'];
    }
}

function displayQuestions(question_number)
{
    var the_sign_options_div = document.getElementById("sign_options_div");
    
    var optionA = document.createElement('div');
    optionA.setAttribute('id', 'optionA');
    optionA.setAttribute('class', 'option');
    
    var optionB = document.createElement('div');
    optionB.setAttribute('id', 'optionB');
    optionB.setAttribute('class', 'option');
    
    var optionC = document.createElement('div');
    optionC.setAttribute('id', 'optionC');
    optionC.setAttribute('class', 'option');
    
    //alert(questions[number]);
    optionA.innerHTML += questions[question_number];
    optionB.innerHTML += questions[question_number + 1];
    optionC.innerHTML += questions[question_number + 2];
    
    the_sign_options_div.appendChild(optionA);
    the_sign_options_div.appendChild(optionB);
    the_sign_options_div.appendChild(optionC);  
}   

function displayVideo()
{
    var the_video_div = document.getElementById("sign_video_div");
    var the_video = document.createElement('video');
    the_video.setAttribute('controls', 'controls');
    the_video.setAttribute('class','video-display');
     the_video.setAttribute('id','sign_video');
    
    the_video.autoplay = true;
    var videoSRC;
    
     var random_number = (Math.random() * 3) + 0 ;
    current_solution = Math.floor(random_number);
    current_solution = current_solution + current_question;
    
    the_video.src = "videos/" + questions_SRC[current_solution];
    the_video_div.appendChild(the_video);
}

function loadCorrectSymbol(option)
{
   $("#" + option).addClass("correct_option");
}

function loadInCorrectSymbol(option)
{
    $("#" + option).addClass("incorrect_option");
}


function disableOption(option)
{
    var the_screen = document.getElementById("screen");
    var optionA = document.getElementById(option);
    the_screen.removeChild(optionA);
    the_screen.removeChild(optionB);
}

function changeOptions(question_number)
{
    var optionA = document.getElementById("optionA");
    var optionB = document.getElementById("optionB");
    var optionC = document.getElementById("optionC");
    
    optionA.innerHTML = questions[question_number];
    $("#optionA").removeClass("incorrect_option");
     $("#optionA").removeClass("correct_option");
    
    optionB.innerHTML = questions[question_number + 1];
    $("#optionB").removeClass("incorrect_option");
     $("#optionB").removeClass("correct_option");
    
    optionC.innerHTML = questions[question_number + 2];
    $("#optionC").removeClass("incorrect_option");
     $("#optionC").removeClass("correct_option");
}

function removeVideo()
{
    var the_sign_video_div = document.getElementById("sign_video_div");
    var the_video = document.getElementById("sign_video");
    
    if(the_video !== null)
        the_sign_video_div.removeChild(the_video);
    
}
