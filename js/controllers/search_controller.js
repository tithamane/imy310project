var search = angular.module('search', []);

search.controller('SearchController', function($scope){
    
    $scope.words = window.DICTIONARY.words;
    $scope.sentences = window.DICTIONARY.sentences;
    
});
